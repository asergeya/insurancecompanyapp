# InsuranceCompanyApp

Coursework.

## Building

This project was developed on GNU/Linux, but it should be able to be built on macOS and Windows.
All that's needed is a C++ development environment and installation of Qt5 or Qt6 framework.

### Requirements

1. C++11 compiler
2. Qt5+
3. MySQL/MariaDB

### GNU/Linux

1. Clone repository: `git clone git@gitlab.com:asergeya/insurancecompanyapp.git`
2. Open src directory: `cd insurancecompanyapp/src`
3. Run qmake. Command may depend on what version of Qt you want to compile under or
   your GNU/Linux distribution (for example: `qmake`).
4. Run make command: `make`.
5. Run app with: `./InsuranceCompanyApp`

## Using

* On first app run you need login in administrator account.
* Add agents. Give credential data for users (agents).
* Now, agents can login, add clients and contract with them.

## TODO

- [ ] Add sign out button
- [ ] Use today's date for contracts
- [ ] Deactivate calc salary button, when row is not selected
- [ ] Deactivate remove button, when row is not selected
- [ ] Store passwords safely (for example, hash + salt)
- [ ] Calc salary only for one month
- [ ] Autogenerate login and password for agents
- [ ] Allow users to change password
- [ ] Translation to Russian
- [ ] Cmake for tex
- [ ] Improve tests

## FAQ

### How can I login in administrator account?

Administrator account is hardcoded in program.
Use *admin* for login and password.
