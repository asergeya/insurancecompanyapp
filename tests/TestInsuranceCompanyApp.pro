QT += testlib
QT += gui sql
CONFIG += qt warn_on depend_includepath testcase

TEMPLATE = app

HEADERS += \
    ../src/database.h

SOURCES += \
    ../src/database.cpp \
    test_database.cpp
