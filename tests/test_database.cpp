#include <QtTest>
#include <QtSql/QSqlError>
#include <QtSql/QSqlTableModel>

#include "../src/database.h"

#define DATABASE_NAME "InsuranceCompanyTest"

Q_DECLARE_METATYPE(Agent);
Q_DECLARE_METATYPE(Client);
Q_DECLARE_METATYPE(Contract);

class TestDatabase : public QObject
{
    Q_OBJECT

public:
    TestDatabase();
    ~TestDatabase();

private slots:
    void test_remove_data();
    void test_addAgent_data();
    void test_addAgent();
    void test_removeAgent_data();
    void test_removeAgent();
    void test_addClient_data();
    void test_addClient();
    void test_removeClient_data();
    void test_removeClient();
    void test_addContract_data();
    void test_addContract();
    void test_removeContract_data();
    void test_removeContract();
    void test_checkLoginCredentials_data();
    void test_checkLoginCredentials();

private:
    QSqlTableModel *model_agents;
    QSqlTableModel *model_clients;
    QSqlTableModel *model_contracts;
};

TestDatabase::TestDatabase()
{
    QSqlError err = initDatabase(DATABASE_NAME);
    if (err.type() != QSqlError::NoError) {
        qDebug() << "Initializing database is failed: " + err.text();
        return;
    }
    model_agents = new QSqlTableModel;
    model_clients = new QSqlTableModel;
    model_contracts = new QSqlTableModel;

    model_agents->setTable("users");
    model_agents->select();
    model_clients->setTable("clients");
    model_clients->select();
    model_contracts->setTable("contracts");
    model_contracts->select();
}

TestDatabase::~TestDatabase()
{
    QSqlQuery query;

    query.exec("drop database " DATABASE_NAME);

    delete model_agents;
    delete model_clients;
    delete model_contracts;
}

void TestDatabase::test_remove_data()
{
    QTest::addColumn<int>("row");
    QTest::addColumn<bool>("result");

    QTest::newRow("negative row 1") << -666 << false;
    QTest::newRow("negative row 2") << -1 << false;
    QTest::newRow("existing row") << 1 << true;
    QTest::newRow("unexisting row") << 1000000 << false;
}

void TestDatabase::test_addAgent_data()
{
    QTest::addColumn<Agent>("agent");
    QTest::addColumn<bool>("result");

    // TODO: improve
    Agent agents[4];

    agents[0].login = "user";
    agents[0].password = "hello";
    agents[1].login = "superuser";
    agents[1].password = "complicated";
    agents[2].login = "user";
    agents[2].password = "difficult";
    agents[3].login = "newuser";
    agents[3].password = "difficult";

    QTest::newRow("agent1") << agents[0] << true;
    QTest::newRow("agent1_duplicate") << agents[0] << false;
    QTest::newRow("agent2") << agents[1] << true;
    QTest::newRow("agent2_duplicate") << agents[1] << false;
    QTest::newRow("agent3_login_duplicate") << agents[2] << false;
    QTest::newRow("agent4_password_duplicate") << agents[3] << true;
}

void TestDatabase::test_addAgent()
{
    QFETCH(Agent, agent);
    QFETCH(bool, result);

    QCOMPARE(addAgent(model_agents, agent), result);
}

void TestDatabase::test_removeAgent_data()
{
    test_remove_data();
}

void TestDatabase::test_removeAgent()
{
    QFETCH(int, row);
    QFETCH(bool, result);

    QCOMPARE(removeAgent(model_agents, row), result);
}

void TestDatabase::test_addClient_data()
{
    QTest::addColumn<Client>("client");
    QTest::addColumn<bool>("result");

    Client client1, client2;

    client1.agentID = "2";
    client1.surname = "Petrov";
    client1.name = "Vasya";
    client1.middle_name = "Userivich";
    client1.address = "Address";
    client1.phone = "0088823324";

    QTest::newRow("client1") << client1 << true;
    QTest::newRow("client1_duplicate") << client1 << false;
}

void TestDatabase::test_addClient()
{
    QFETCH(Client, client);
    QFETCH(bool, result);

    QCOMPARE(addClient(model_clients, client), result);
}

void TestDatabase::test_removeClient_data()
{
    test_remove_data();
}

void TestDatabase::test_removeClient()
{
    QFETCH(int, row);
    QFETCH(bool, result);

    QCOMPARE(removeClient(model_clients, row), result);
}

void TestDatabase::test_addContract_data()
{
    QTest::addColumn<Contract>("contract");
    QTest::addColumn<bool>("result");

    Contract contract1;

    contract1.agentID = "2";
    contract1.clientID = "3";
    contract1.type = "Home property insurance";
    contract1.sum = "1000";
    contract1.rate = "1.1";
    contract1.date = "28.11.21";

    QTest::newRow("contract1") << contract1 << true;
    QTest::newRow("contract1_duplicate") << contract1 << false;
}

void TestDatabase::test_addContract()
{
    QFETCH(Contract, contract);
    QFETCH(bool, result);

    QCOMPARE(addContract(model_contracts, contract), result);
}

void TestDatabase::test_removeContract_data()
{
    test_remove_data();
}

void TestDatabase::test_removeContract()
{
    QFETCH(int, row);
    QFETCH(bool, result);

    QCOMPARE(removeContract(model_clients, row), result);
}

void TestDatabase::test_checkLoginCredentials_data()
{
    QTest::addColumn<QString>("login");
    QTest::addColumn<QString>("password");
    QTest::addColumn<bool>("result");

    QTest::newRow("correct") << "admin" << "admin" << true;
    QTest::newRow("correct") << "newuser" << "difficult" << true;
    QTest::newRow("wrong password") << "admin" << "user" << false;
    QTest::newRow("wrong password and login") << "badadmin" << "user" << false;
}

void TestDatabase::test_checkLoginCredentials()
{
    QFETCH(QString, login);
    QFETCH(QString, password);
    QFETCH(bool, result);

    QCOMPARE(checkLoginCredentials(login, password), result);
}

QTEST_MAIN(TestDatabase)

#include "test_database.moc"
