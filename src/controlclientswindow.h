#ifndef CONTROLCLIENTSWINDOW_H
#define CONTROLCLIENTSWINDOW_H

#include "addclientwindow.h"
#include "controlcontractswindow.h"

#include <QMainWindow>

namespace Ui {
class ControlClientsWindow;
}

class ControlClientsWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit ControlClientsWindow(QString agent_id, QWidget *parent = nullptr);
    ~ControlClientsWindow();

private slots:
    void displayAddClientWindow();
    void displayControlContractsWindow();
    void removeClientButton();

private:
    Ui::ControlClientsWindow *ui;
    AddClientWindow *mAddClientWindow;
    ControlContractsWindow *mControlContractsWindow;
    QString mAgentID;
    QSqlTableModel *model;

    void setupModel(const QString &tablename, const QStringList &headers);
    void createUi();
};

#endif // CONTROLCLIENTSWINDOW_H
