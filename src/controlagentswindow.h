#ifndef CONTROLAGENTSWINDOW_H
#define CONTROLAGENTSWINDOW_H

#include "addagentwindow.h"

#include <QMainWindow>

namespace Ui {
class ControlAgentsWindow;
}

class ControlAgentsWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit ControlAgentsWindow(QWidget *parent = nullptr);
    ~ControlAgentsWindow();

private slots:
    void displayAddAgentWindow();
    void removeAgentButton();
    void displaySalaryButton();

private:
    Ui::ControlAgentsWindow *ui;
    AddAgentWindow *mAddAgentWindow;
    QSqlTableModel *model;

    void setupModel(const QString &tablename, const QStringList &headers);
    void createUi();
};

#endif // CONTROLAGENTSWINDOW_H
