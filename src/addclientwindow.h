#ifndef ADDCLIENTWINDOW_H
#define ADDCLIENTWINDOW_H

#include "database.h"

#include <QWidget>

namespace Ui {
class AddClientWindow;
}

class AddClientWindow : public QWidget
{
    Q_OBJECT

public:
    explicit AddClientWindow(QSqlTableModel *model, QString agent_id, QWidget *parent = nullptr);
    ~AddClientWindow();

private slots:
    void addClientButton();

private:
    Ui::AddClientWindow *ui;
    QString mAgentID;
    QSqlTableModel *model;

    bool readClientData(Client &client);
};

#endif // ADDCLIENTWINDOW_H
