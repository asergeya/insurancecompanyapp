/*! 
 * \file database.h
 * \brief In this file implemented function, which works with database.
 * Here are declared structs for agent, client and contract.
 * Also, there are global variables, which contains SQL queries. 
 */

#ifndef DATABASE_H
#define DATABASE_H

#include <QtSql>
#include <QString>

#define SALARY_PERCENT "0.1"

struct Agent
{
    QString login;
    QString password;
    QString surname;
    QString name;
    QString middle_name;
    QString address;
    QString branch;
    QString phone;
    QString department;
};

struct Client
{
    QString agentID;
    QString surname;
    QString name;
    QString middle_name;
    QString address;
    QString phone;
};

struct Contract
{
   QString agentID;
   QString clientID;
   QString type;
   QString sum;
   QString rate;
   QString date;
};

//! This variable contains SQL query for creation database
const auto CREATE_DATABASE_SQL = QLatin1String(R"(
    create database if not exists
    )");

//! This variable contains use SQL query
const auto USE_DATABASE_SQL = QLatin1String(R"(
    use
    )");

//! This variable contains SQL query, which creates table for users in database
const auto USERS_SQL = QLatin1String(R"(
    create table if not exists users
    (id int not null auto_increment primary key,
    surname varchar(20),
    name varchar(20),
    middle_name varchar(20),
    address varchar(255),
    phone varchar(13),
    branch varchar(20),
    login varchar(20) unique key,
    password varchar(20))
    )");

//! This variable contains SQL query, which creates table for clients in database
const auto CLIENTS_SQL = QLatin1String(R"(
    create table if not exists clients
    (id int not null auto_increment primary key,
    id_agent int not null,
    surname varchar(20),
    name varchar(20),
    middle_name varchar(20),
    address varchar(255),
    phone varchar(13))
    )");

//! This variable contains SQL query, which creates table for contracts in database
const auto CONTRACTS_SQL = QLatin1String(R"(
    create table if not exists contracts
    (id int not null auto_increment primary key,
    id_agent int not null,
    id_client int not null,
    type varchar(50),
    sum int,
    rate float,
    date date)
    )");

//! This variable contains SQL query, which inserts administrator account in users table
const auto INSERT_ADMIN_SQL = QLatin1String(R"(
    insert into users(id, login, password) values(0, 'admin', 'admin')
    )");

//! This variable contains SQL query, which selects admin's record.
const auto CHECK_ADMIN_SQL = QLatin1String(R"(
    select * from users where login = 'admin'
    )");

//! Create connection with database, create necessary tables and insert admin account
QSqlError initDatabase(QString dbname = "InsuranceCompany");
bool checkLoginCredentials(const QString &login, const QString &password);
QString getAgentID(const QString &login);
bool addAgent(QSqlTableModel *model, Agent &agent);
bool removeAgent(QSqlTableModel *model, int row);
QString getAgentSalary(QSqlTableModel *model, int row);
bool addClient(QSqlTableModel *model, Client &client);
bool removeClient(QSqlTableModel *model, int row);
bool addContract(QSqlTableModel *model, Contract &contract);
bool removeContract(QSqlTableModel *model, int row);

#endif // DATABASE_H
