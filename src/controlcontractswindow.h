#ifndef CONTROLCONTRACTSWINDOW_H
#define CONTROLCONTRACTSWINDOW_H

#include "addcontractwindow.h"

#include <QMainWindow>

namespace Ui {
class ControlContractsWindow;
}

class ControlContractsWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit ControlContractsWindow(QString agent_id, QString client_id, QWidget *parent = nullptr);
    ~ControlContractsWindow();

private slots:
    void displayAddContractWindow();
    void removeContractButton();

private:
    Ui::ControlContractsWindow *ui;
    AddContractWindow *mAddContractWindow;
    QSqlTableModel *model;
    QString mAgentID;
    QString mClientID;

    void setupModel(const QString &tablename, const QStringList &headers);
    void createUi();
};

#endif // CONTROLCONTRACTSWINDOW_H
