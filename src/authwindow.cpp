#include "authwindow.h"
#include "ui_authwindow.h"
#include "database.h"

#include <QMessageBox>

AuthWindow::AuthWindow(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::AuthWindow)
{
    ui->setupUi(this);

    QSqlError err = initDatabase();
    if (err.type() != QSqlError::NoError) {
        QMessageBox::critical(this, "Unable to initialize Database",
                        "Error initializing database: " + err.text());
        return;
    }
}

AuthWindow::~AuthWindow()
{
    delete ui;
}

void AuthWindow::signInButton()
{
    QString login = ui->loginLineEdit->text().toLower();
    QString password = ui->passwordLineEdit->text();

    if (checkLoginCredentials(login, password)) {
        if (login == "admin") {
            displayAgentsControlWindow();
        }
        else {
            displayClientsControlWindow(getAgentID(login));
        }
    }
    else {
        QMessageBox msg;
        msg.setText(tr("The login or password is incorrect!"));
        msg.exec();
    }
}

inline void AuthWindow::displayAgentsControlWindow()
{
    this->close();
    mControlAgentsWindow = new ControlAgentsWindow;
    mControlAgentsWindow->show();
}

inline void AuthWindow::displayClientsControlWindow(QString agent_id)
{
    this->close();
    mControlClientsWindow = new ControlClientsWindow(agent_id);
    mControlClientsWindow->show();
}
