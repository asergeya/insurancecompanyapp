#ifndef ADDAGENTWINDOW_H
#define ADDAGENTWINDOW_H

#include "database.h"

#include <QWidget>

namespace Ui {
class AddAgentWindow;
}

class AddAgentWindow : public QWidget
{
    Q_OBJECT

public:
    explicit AddAgentWindow(QSqlTableModel *model, QWidget *parent = nullptr);
    ~AddAgentWindow();

private slots:
    void addAgentButton();

private:
    Ui::AddAgentWindow *ui;
    QSqlTableModel *model;

    bool readAgentData(Agent &agent);
};

#endif // ADDAGENTWINDOW_H
