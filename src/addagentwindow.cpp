#include "addagentwindow.h"
#include "ui_addagentwindow.h"

#include <QMessageBox>

AddAgentWindow::AddAgentWindow(QSqlTableModel *model, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AddAgentWindow)
{
    ui->setupUi(this);
    this->model = model;
}

AddAgentWindow::~AddAgentWindow()
{
    delete ui;
}

void AddAgentWindow::addAgentButton()
{
    QMessageBox msg;
    Agent agent;

    if (!readAgentData(agent)) {
        msg.setText(tr("All fields must be filled!"));
        msg.exec();
        return;
    }
    if (addAgent(model, agent)) {
        this->close();
    }
    else {
        msg.setText("Can't insert\n" + model->lastError().text());
        msg.exec();
    }
}

bool AddAgentWindow::readAgentData(Agent &agent)
{
    if (ui->nameLineEdit->text().isEmpty() or
        ui->surnameLineEdit->text().isEmpty() or
        ui->addressLineEdit->text().isEmpty() or
        ui->phoneLineEdit->text().isEmpty() or
        ui->loginLineEdit->text().isEmpty() or
        ui->branchLineEdit->text().isEmpty() or
        ui->passwordLineEdit->text().isEmpty()) {

        return false;
    }
    agent.surname = ui->surnameLineEdit->text();
    agent.name = ui->nameLineEdit->text();
    agent.middle_name = ui->middleNameLineEdit->text();
    agent.address = ui->addressLineEdit->text();
    agent.phone = ui->phoneLineEdit->text();
    agent.branch = ui->branchLineEdit->text();
    agent.login = ui->loginLineEdit->text();
    agent.password = ui->passwordLineEdit->text();

    return true;
}
