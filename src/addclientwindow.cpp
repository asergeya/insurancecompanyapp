#include "addclientwindow.h"
#include "ui_addclientwindow.h"

#include <QMessageBox>

AddClientWindow::AddClientWindow(QSqlTableModel *model, QString agent_id, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AddClientWindow)
{
    ui->setupUi(this);
    mAgentID = agent_id;
    this->model = model;
}

AddClientWindow::~AddClientWindow()
{
    delete ui;
}

void AddClientWindow::addClientButton()
{
    QMessageBox msg;
    Client client;

    if (!readClientData(client)) {
        msg.setText(tr("All fields must be filled!"));
        msg.exec();
        return;
    }
    if (addClient(model, client)) {
        this->close();
    }
    else {
        msg.setText("Can't insert\n" + model->lastError().text());
        msg.exec();
    }
}

bool AddClientWindow::readClientData(Client &client)
{
    if (ui->nameLineEdit->text().isEmpty() or
        ui->surnameLineEdit->text().isEmpty() or
        ui->addressLineEdit->text().isEmpty() or
        ui->phoneLineEdit->text().isEmpty()) {

        return false;
    }
    client.agentID = mAgentID;
    client.surname = ui->surnameLineEdit->text();
    client.name = ui->nameLineEdit->text();
    client.middle_name = ui->middleNameLineEdit->text();
    client.address = ui->addressLineEdit->text();
    client.phone = ui->phoneLineEdit->text();

    return true;
}
