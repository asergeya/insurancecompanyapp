#include "controlagentswindow.h"
#include "ui_controlagentswindow.h"
#include "database.h"

#include <QMessageBox>

ControlAgentsWindow::ControlAgentsWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ControlAgentsWindow)
{
    ui->setupUi(this);

    setupModel("users", QStringList() << tr("Id")
                                      << tr("Surname")
                                      << tr("Name")
                                      << tr("Middle name")
                                      << tr("Address")
                                      << tr("Branch")
                                      << tr("Phone")
                                      << tr("Login")
    );
    createUi();
}

ControlAgentsWindow::~ControlAgentsWindow()
{
    delete ui;
}

void ControlAgentsWindow::displayAddAgentWindow()
{
    mAddAgentWindow = new AddAgentWindow(model);
    mAddAgentWindow->show();
}

void ControlAgentsWindow::removeAgentButton()
{
    auto row = ui->tableView->currentIndex().row();

    if (removeAgent(model, row)) {
        ui->tableView->setRowHidden(row, true);
    }
}

void ControlAgentsWindow::displaySalaryButton()
{
    QMessageBox msg;
    auto row = ui->tableView->currentIndex().row();

    if (row != -1) {
        auto salary = getAgentSalary(model, row);
        msg.setText("Agent salary is " + salary + "\n");
        msg.exec();
    }
}

void ControlAgentsWindow::setupModel(const QString &tablename, const QStringList &headers)
{
    model = new QSqlTableModel;
    model->setTable(tablename);
    model->setEditStrategy(QSqlTableModel::OnFieldChange);

    for (auto i = 0; i < model->columnCount() - 1; i++) {
        model->setHeaderData(i, Qt::Horizontal, headers[i]);
    }
}

void ControlAgentsWindow::createUi()
{
    ui->tableView->setModel(model);
    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableView->verticalHeader()->setHidden(true);
    ui->tableView->hideColumn(model->fieldIndex("id"));
    ui->tableView->hideColumn(model->fieldIndex("password"));
    ui->tableView->hideRow(0); // admin record

    model->select();
}
