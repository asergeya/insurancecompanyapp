#include "controlclientswindow.h"
#include "ui_controlclientswindow.h"
#include "database.h"

ControlClientsWindow::ControlClientsWindow(QString agent_id, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ControlClientsWindow),
    mAgentID(agent_id)
{
    ui->setupUi(this);
    setupModel("clients", QStringList() << tr("Id")
                                        << tr("AgentId")
                                        << tr("Surname")
                                        << tr("Name")
                                        << tr("Middle name")
                                        << tr("Address")
                                        << tr("Phone")
    );
    createUi();
}

ControlClientsWindow::~ControlClientsWindow()
{
    delete ui;
}

void ControlClientsWindow::displayAddClientWindow()
{
    mAddClientWindow = new AddClientWindow(model, mAgentID);
    mAddClientWindow->show();
}

void ControlClientsWindow::displayControlContractsWindow()
{
    auto row = ui->tableView->currentIndex().row();

    if (row != -1) {
        QString clientID = model->record(row).value("id").toString();

        mControlContractsWindow = new ControlContractsWindow(mAgentID, clientID);
        mControlContractsWindow->show();
    }
}

void ControlClientsWindow::removeClientButton()
{
    auto row = ui->tableView->currentIndex().row();

    if (removeClient(model, row)) {
        ui->tableView->setRowHidden(row, true);
    }
}

void ControlClientsWindow::setupModel(const QString &tablename, const QStringList &headers)
{
    model = new QSqlTableModel;
    model->setTable(tablename);
    model->setEditStrategy(QSqlTableModel::OnFieldChange);
    model->setFilter("id_agent = '" + mAgentID + "'");

    for (auto i = 0; i < model->columnCount(); i++) {
        model->setHeaderData(i, Qt::Horizontal, headers[i]);
    }
}

void ControlClientsWindow::createUi()
{
    ui->tableView->setModel(model);
    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableView->verticalHeader()->setHidden(true);
    ui->tableView->hideColumn(model->fieldIndex("id"));
    ui->tableView->hideColumn(model->fieldIndex("id_agent"));

    model->select();
}
