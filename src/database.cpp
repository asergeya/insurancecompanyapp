#include "database.h"

QSqlError initDatabase(QString dbname)
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL");
    // Change to the appropriate data
    db.setHostName("localhost");
    db.setUserName("qtuser");
    db.setPassword("12345");

    if (!db.open())
        return db.lastError();

    QSqlQuery query;

    if (!query.exec(CREATE_DATABASE_SQL + dbname)) {
        return query.lastError();
    }
    if (!query.exec(USE_DATABASE_SQL + dbname)) {
        return query.lastError();
    }
    if (!query.exec(USERS_SQL)) {
        return query.lastError();
    }
    if (!query.exec(CLIENTS_SQL)) {
        return query.lastError();
    }
    if (!query.exec(CONTRACTS_SQL)) {
        return query.lastError();
    }
    if (!query.exec(CHECK_ADMIN_SQL)) {
        return query.lastError();
    }
    query.next();
    if (!query.isValid()) {
        query.exec(INSERT_ADMIN_SQL);
    }

    return QSqlError();
}

bool checkLoginCredentials(const QString &login, const QString &password)
{
    QSqlQuery query;

    query.exec("select password from users where login = '" + login + "'");
    query.next();

    return (query.isValid() and query.value(0).toString() == password);
}

QString getAgentID(const QString &login)
{
    QSqlQuery query;

    query.exec("select id from users where login = '" + login + "'");
    query.next();

    return query.value(0).toString();
}

bool addAgent(QSqlTableModel *model, Agent &agent)
{
    QSqlRecord record = model->record();

    record.remove(record.indexOf("id"));
    record.setValue("surname", agent.surname);
    record.setValue("name", agent.name);
    record.setValue("middle_name", agent.middle_name);
    record.setValue("address", agent.address);
    record.setValue("phone", agent.phone);
    record.setValue("branch", agent.branch);
    record.setValue("login", agent.login);
    record.setValue("password", agent.password);

    if (model->insertRecord(-1, record)) {
        model->submitAll();

        return true;
    }
    return false;
}

bool removeAgent(QSqlTableModel *model, int row)
{
    if (row >= 0 and row <= model->rowCount()) {
        QString agentID = model->record(row).value("id").toString();
        QSqlQuery query;

        query.exec("delete from contracts where id_agent = '" + agentID + "'");
        query.exec("delete from clients where id_agent = '" + agentID + "'");

        model->removeRow(row);
        model->submitAll();

        return true;
    }
    return false;
}

QString getAgentSalary(QSqlTableModel *model, int row)
{
    if (row >= 0 and row <= model->rowCount()) {
        QString agentID = model->record(row).value("id").toString();
        QSqlQuery query;

        query.exec("select round(sum(sum*rate*" SALARY_PERCENT ")) "
                   "from contracts where id_agent = '" + agentID + "'");
        query.next();

        return query.value(0).toString();
    }
}

bool addClient(QSqlTableModel *model, Client &client)
{
    QSqlRecord record = model->record();

    record.remove(record.indexOf("id"));
    record.setValue("id_agent", client.agentID);
    record.setValue("surname", client.surname);
    record.setValue("name", client.name);
    record.setValue("middle_name", client.middle_name);
    record.setValue("address", client.address);
    record.setValue("phone", client.phone);

    if (model->insertRecord(-1, record)) {
        model->submitAll();

        return true;
    }
    return false;
}
bool removeClient(QSqlTableModel *model, int row)
{
    if (row >= 0 and row <= model->rowCount()) {
        QString clientID = model->record(row).value("id").toString();
        QSqlQuery query;

        query.exec("delete from contracts where id_client = '" + clientID + "'");

        model->removeRow(row);
        model->submitAll();

        return true;
    }
    return false;
}

bool addContract(QSqlTableModel *model, Contract &contract)
{
    QSqlRecord record = model->record();

    record.remove(record.indexOf("id"));
    record.setValue("id_agent", contract.agentID);
    record.setValue("id_client", contract.clientID);
    record.setValue("type", contract.type);
    record.setValue("sum", contract.sum);
    record.setValue("rate", contract.rate);
    record.setValue("date", contract.date);

    if (model->insertRecord(-1, record)) {
        model->submitAll();

        return true;
    }
    return false;
}
bool removeContract(QSqlTableModel *model, int row)
{
    if (row >= 0 and row <= model->rowCount()) {
        model->removeRow(row);
        model->submitAll();

        return true;
    }
    return false;
}
