QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11 console

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    main.cpp \
    authwindow.cpp \
    database.cpp \
    controlagentswindow.cpp \
    controlclientswindow.cpp \
    controlcontractswindow.cpp \
    addagentwindow.cpp \
    addclientwindow.cpp \
    addcontractwindow.cpp

HEADERS += \
    authwindow.h \
    database.h \
    controlagentswindow.h \
    controlclientswindow.h \
    controlcontractswindow.h \
    addagentwindow.h \
    addclientwindow.h \
    addcontractwindow.h

FORMS += \
    authwindow.ui \
    controlagentswindow.ui \
    controlclientswindow.ui \
    controlcontractswindow.ui \
    addagentwindow.ui \
    addclientwindow.ui \
    addcontractwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
