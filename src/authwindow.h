#ifndef AUTHWINDOW_H
#define AUTHWINDOW_H

#include "controlagentswindow.h"
#include "controlclientswindow.h"

#include <QWidget>

QT_BEGIN_NAMESPACE
namespace Ui { class AuthWindow; }
QT_END_NAMESPACE

class AuthWindow : public QWidget
{
    Q_OBJECT

public:
    AuthWindow(QWidget *parent = nullptr);
    ~AuthWindow();

private slots:
    void signInButton();

private:
    Ui::AuthWindow *ui;
    ControlAgentsWindow *mControlAgentsWindow;
    ControlClientsWindow *mControlClientsWindow;

    void displayAgentsControlWindow();
    void displayClientsControlWindow(QString agent_id);
};
#endif // AUTHWINDOW_H
