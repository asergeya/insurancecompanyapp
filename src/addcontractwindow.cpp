#include "addcontractwindow.h"
#include "ui_addcontractwindow.h"

#include <QMessageBox>

AddContractWindow::AddContractWindow(QSqlTableModel *model, QString agent_id, QString client_id, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AddContractWindow)
{
    ui->setupUi(this);
    mAgentID = agent_id;
    mClientID = client_id;
    this->model = model;
}

AddContractWindow::~AddContractWindow()
{
    delete ui;
}

void AddContractWindow::addContractButton()
{
    QMessageBox msg;
    Contract contract;

    if (!readContractData(contract)) {
        msg.setText(tr("All fields must be filled!"));
        msg.exec();
        return;
    }
    if (addContract(model, contract)) {
        this->close();
    }
    else {
        msg.setText("Can't insert\n" + model->lastError().text());
        msg.exec();
    }
}

bool AddContractWindow::readContractData(Contract &contract)
{
    if (ui->typeComboBox->currentIndex() == -1 or
        ui->sumSpinBox->value() <= 0 or
        ui->rateDoubleSpinBox->value() <= 0) {

        return false;
    }
    contract.agentID = mAgentID;
    contract.clientID = mClientID;
    contract.type = ui->typeComboBox->currentText();
    contract.sum = ui->sumSpinBox->text();
    contract.rate = ui->rateDoubleSpinBox->text();
    contract.date = ui->dateEdit->text();

    return true;
}
