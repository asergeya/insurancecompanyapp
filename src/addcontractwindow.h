#ifndef ADDCONTRACTWINDOW_H
#define ADDCONTRACTWINDOW_H

#include "database.h"

#include <QWidget>

namespace Ui {
class AddContractWindow;
}

class AddContractWindow : public QWidget
{
    Q_OBJECT

public:
    explicit AddContractWindow(QSqlTableModel *model, QString agent_id, QString client_id, QWidget *parent = nullptr);
    ~AddContractWindow();

private slots:
    void addContractButton();

private:
    Ui::AddContractWindow *ui;
    QSqlTableModel *model;
    QString mAgentID;
    QString mClientID;

    bool readContractData(Contract &contract);
};

#endif // ADDCONTRACTWINDOW_H
