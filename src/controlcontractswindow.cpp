#include "controlcontractswindow.h"
#include "ui_controlcontractswindow.h"

ControlContractsWindow::ControlContractsWindow(QString agent_id, QString client_id, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ControlContractsWindow),
    mAgentID(agent_id),
    mClientID(client_id)
{
    ui->setupUi(this);
    setupModel("contracts", QStringList() << tr("Id")
                                          << tr("AgentId")
                                          << tr("ClientId")
                                          << tr("Type")
                                          << tr("Sum")
                                          << tr("Rate")
                                          << tr("Date")
    );
    createUi();
}

ControlContractsWindow::~ControlContractsWindow()
{
    delete ui;
}

void ControlContractsWindow::displayAddContractWindow()
{
    mAddContractWindow = new AddContractWindow(model, mAgentID, mClientID);
    mAddContractWindow->show();
}

void ControlContractsWindow::removeContractButton()
{
    auto row = ui->tableView->currentIndex().row();

    if (removeContract(model, row)) {
        ui->tableView->setRowHidden(row, true);
    }
}

void ControlContractsWindow::setupModel(const QString &tablename, const QStringList &headers)
{
    model = new QSqlTableModel;
    model->setTable(tablename);
    model->setEditStrategy(QSqlTableModel::OnFieldChange);
    model->setFilter("id_client = '" + mClientID + "'");

    for (auto i = 0; i < model->columnCount(); i++) {
        model->setHeaderData(i, Qt::Horizontal, headers[i]);
    }
}

void ControlContractsWindow::createUi()
{
    ui->tableView->setModel(model);
    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableView->verticalHeader()->setHidden(true);
    ui->tableView->hideColumn(model->fieldIndex("id"));
    ui->tableView->hideColumn(model->fieldIndex("id_agent"));
    ui->tableView->hideColumn(model->fieldIndex("id_client"));

    model->select();
}
