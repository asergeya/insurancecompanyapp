var indexSectionsWithContent =
{
  0: "abcdfghilmnprstuv~",
  1: "acu",
  2: "u",
  3: "acdmu",
  4: "acgimrs~",
  5: "abcdfhilmnprstuv",
  6: "s"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Macros"
};

